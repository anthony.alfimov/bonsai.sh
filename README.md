# bonsai.sh

![](previews/bonsai.png)

`bonsai.sh` is a bonsai tree generator for the terminal, written in `bash`. It supports both live and static generation.

# Installation

To clone to `~/bin/bonsai.sh`, make executable, and add to available commands:

```bash
git clone https://gitlab.com/jallbrit/bonsai.sh ~/bin/bonsai.sh
chmod +x ~/bin/bonsai.sh/bonsai.sh
ln -s bonsai.sh/bonsai.sh ~/.local/bin/bonsai
```

# Usage

```
Usage: bonsai [-h] [-i] [-l] [-T] [-m message] [-t time] 
              [-g x,y] [ -c char] [-M 0-9]

bonsai.sh is a static and live bonsai tree generator, written in bash.

optional args:
  -l, --live             enable live generation
  -t, --time time        time between each step of growth [default: 0.01]
  -m, --message text     attach a message to the tree
  -b, --basetype 0-2     which ascii-art plant base to use (0 for none) [default: 1]
  -i, --infinite         keep generating trees until quit (2s between each)
  -T, --termcolors       use terminal colors
  -g, --geo geo          set custom geometry [default: fit to terminal]
  -c, --leaf char        character used for leaves [default: &]
  -M, --multiplier 0-9   branch multiplier; higher equals more branching [default: 5]
  -L, --life int         life of tree; higher equals more overall growth [default: 28]
  -h, --help             show help
```

# Examples

## `bonsai -l -b 2`

![bonsai -Ti](previews/bonsai-lb2.gif)

## `bonsai -T -m "$(fortune)"`

![bonsai -l](previews/bonsai-Tm.png)
